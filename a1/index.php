<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S04: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Access Modifiers</h1>

	<h2>Building Variables</h2>

	<p><?php // echo $building->name; ?></p>

	<h2>Condominium Variables</h2>

	<p><?php // echo $condominium->name; ?></p>

	<h1>Encapsulation</h1>

	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

	<?php $condominium->setName('Enzo Tower') ?>

	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>

	<h2>Building</h2>

	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
	
	<p><?php echo $building->getName(); ?> Building has <?php echo $building->getFloor(); ?> floors.</p>
	
	<p><?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>
	
	<?php $building->setName('Caswynn Complex') ?>
	
	<p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

	<h2>Condominium</h2>

	<?php $condominium->setName('Enzo Condo') ?>

	<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
	
	<p><?php echo $condominium->getName(); ?> condominium has <?php echo $condominium->getFloor(); ?> floors.</p>
	
	<p><?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>
	
	<?php $condominium->setName('Enzo Tower') ?>
	
	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>
</body>
</html>